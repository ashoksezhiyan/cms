document.getElementById("analysiscredittotal").onclick=function(){credittotalFunc(this)};

function credittotalFunc(id) {
    var xmlhttp;

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("GET","analysis/credittotal.php",true);
    xmlhttp.send();
}

document.getElementById("analysiscreditbeatwise").onclick=function(){creditbeatwiseFunc(this)};

function creditbeatwiseFunc(id) {
    var xmlhttp;

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("GET","analysis/creditbeatwise.php",true);
    xmlhttp.send();
}

/* function to handle Beat Addition Form
   we have to return false in this function
   to avoid form submit action not to happen
   Required action is already taken in this
   onsubmit event
*/
function creditbeatwise(id) {
    var xmlhttp;
    var text = "";

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("result").innerHTML=xmlhttp.responseText;
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("POST","analysis/creditbeatwisefromDB.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    text += ("beatname=" + id.options[id.selectedIndex].value);
    xmlhttp.send(text);
    return false;
} 

function printData(divToPrint)
{
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
}
