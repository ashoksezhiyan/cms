document.getElementById("beatview").onclick=function(){beatviewFunc(this)};

function beatviewFunc(id) {
    var xmlhttp;

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("GET","beat/beatview.php",true);
    xmlhttp.send();
}

document.getElementById("beatadd").onclick=function(){beataddFunc(this)};

function beataddFunc(id) {
    var xmlhttp;

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("GET","beat/beatadd.php",true);
    xmlhttp.send();
}

/* function to handle Beat Addition Form
   we have to return false in this function
   to avoid form submit action not to happen
   Required action is already taken in this
   onsubmit event
*/
function addnewbeat(id) {
    var xmlhttp;
    var text = "";

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("POST","beat/beataddtoDB.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    for (i = 0; i < id.length; i++) {
        text += id.elements[i].name + "=";
        text += id.elements[i].value + "&";
    }
    xmlhttp.send(text);
    return false;
} 

document.getElementById("beatdel").onclick=function(){beatdelFunc(this)};

function beatdelFunc(id) {
    var xmlhttp;

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("GET","beat/beatdel.php",true);
    xmlhttp.send();
}

/* function to handle Beat Addition Form
   we have to return false in this function
   to avoid form submit action not to happen
   Required action is already taken in this
   onsubmit event
*/
function delbeat(id) {
    var xmlhttp;
    var text = "";

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("POST","beat/beatdelfromDB.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    for (i = 0; i < id.length; i++) {
        text += id.elements[i].name + "=";
        text += id.elements[i].value + "&";
    }
    xmlhttp.send(text);
    return false;
} 
