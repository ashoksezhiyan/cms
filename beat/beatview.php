<?php
$host="localhost"; // Host name
$username="root"; // Mysql username
$password="confirm"; // Mysql password
$db_name="SwasthikAgencies"; // Database name

$con=mysqli_connect("$host","$username","$password","$db_name");
// Check connection
if (mysqli_connect_errno()) {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$result = mysqli_query($con,"SELECT * FROM BeatTable ORDER BY FIELD(DayOfWeek, 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday')");

if (mysqli_num_rows($result) == 0) {
    mysqli_close($con);
    die("No data found");
}

echo "<table border='1'>
<tr>
<th>Beat Name</th>
<th>Sales Person</th>
<th>Visit Day</th>
</tr>";

while($row = mysqli_fetch_array($result)) {
  echo "<tr>";
  echo "<td>" . $row['Name'] . "</td>";
  echo "<td>" . $row['Salesman'] . "</td>";
  echo "<td>" . $row['DayOfWeek'] . "</td>";
  echo "</tr>";
}

echo "</table>";

mysqli_close($con);
?> 
