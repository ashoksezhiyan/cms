document.getElementById("collectionview").onclick=function(){collectionviewFunc(this)};

function collectionviewFunc(id) {
    var xmlhttp;

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("GET","collection/collectionview.php",true);
    xmlhttp.send();
}

document.getElementById("collectionadd").onclick=function(){collectionaddFunc(this)};

function collectionaddFunc(id) {
    var xmlhttp;

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("GET","collection/collectionadd.php",true);
    xmlhttp.send();
}

/* function to handle Beat Addition Form
   we have to return false in this function
   to avoid form submit action not to happen
   Required action is already taken in this
   onsubmit event
*/
function addnewcollection(id) {
    var xmlhttp;
    var text = "";

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("POST","collection/collectionaddtoDB.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    for (i = 0; i < id.length; i++) {
        text += id.elements[i].name + "=";
        text += id.elements[i].value + "&";
    }
    xmlhttp.send(text);
    return false;
} 

document.getElementById("collectiondel").onclick=function(){collectiondelFunc(this)};

function collectiondelFunc(id) {
    var xmlhttp;

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("GET","collection/collectiondel.php",true);
    xmlhttp.send();
}

/* function to handle Beat Addition Form
   we have to return false in this function
   to avoid form submit action not to happen
   Required action is already taken in this
   onsubmit event
*/
function delcollection(id) {
    var xmlhttp;
    var text = "";

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("POST","collection/collectiondelfromDB.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    for (i = 0; i < id.length; i++) {
        text += id.elements[i].name + "=";
        text += id.elements[i].value + "&";
    }
    xmlhttp.send(text);
    return false;
} 

function printData(divToPrint)
{
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
}
