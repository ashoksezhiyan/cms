document.getElementById("daybookview").onclick=function(){daybookviewFunc(this)};

function daybookviewFunc(id) {
    var xmlhttp;

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
            $("#datepicker").datepicker();
        }
    }
    xmlhttp.open("GET","daybook/daybookview.php",true);
    xmlhttp.send();
}


/* function to handle Beat Addition Form
   we have to return false in this function
   to avoid form submit action not to happen
   Required action is already taken in this
   onsubmit event
*/
function viewdaybook(id) {
    var xmlhttp;
    var text = "";

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("result").innerHTML=xmlhttp.responseText;
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("POST","daybook/daybookviewfromDB.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    for (i = 0; i < id.length; i++) {
        text += id.elements[i].name + "=";
        text += id.elements[i].value + "&";
    }
    xmlhttp.send(text);
    return false;
} 


document.getElementById("daybookviewbillno").onclick=function(){daybookviewbillnoFunc(this)};

function daybookviewbillnoFunc(id) {
    var xmlhttp;

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("GET","daybook/daybookviewbillno.php",true);
    xmlhttp.send();
}


/* function to handle Beat Addition Form
   we have to return false in this function
   to avoid form submit action not to happen
   Required action is already taken in this
   onsubmit event
*/
function viewdaybookbybillno(id) {
    var xmlhttp;
    var text = "";

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("result").innerHTML=xmlhttp.responseText;
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("POST","daybook/daybookviewbillnofromDB.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    for (i = 0; i < id.length; i++) {
        text += id.elements[i].name + "=";
        text += id.elements[i].value + "&";
    }
    xmlhttp.send(text);
    return false;
} 

document.getElementById("daybookdeletebillno").onclick=function(){daybookdeletebillnoFunc(this)};

function daybookdeletebillnoFunc(id) {
    var xmlhttp;

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("GET","daybook/daybookdeletebillno.php",true);
    xmlhttp.send();
}


/* function to handle Beat Addition Form
   we have to return false in this function
   to avoid form submit action not to happen
   Required action is already taken in this
   onsubmit event
*/
function deletedaybookbybillno(id) {
    var xmlhttp;
    var text = "";

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("result").innerHTML=xmlhttp.responseText;
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("POST","daybook/daybookdeletebillnofromDB.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    for (i = 0; i < id.length; i++) {
        text += id.elements[i].name + "=";
        text += id.elements[i].value + "&";
    }
    xmlhttp.send(text);
    return false;
} 
