<?php
session_start();
if((!isset($_SESSION['myusername']))
   || (!isset($_SESSION['mypassword'])))
{
header("location:home.html");
}
?>

<!DOCTYPE html>
<html>

<head>
<link rel="stylesheet" type="text/css" href="mystyle.css">
<link rel="stylesheet" href="jquery-ui.css">
</head>

<title>RS CMS</title>
<body>

<div id="container">

    <div id="header">
        <h1> RS - Credit Management System</h1>
    </div>

    <div id="menu">
        <a class="menuhead" id="analysishead">Analysis</a><br> 
          <a class="menuitem" id="analysiscredittotal">Credit - Total</a><br>
          <a class="menuitem" id="analysiscreditbeatwise">Credit - Beatwise</a><br>
        <a class="menuhead" id="daybookhead">Day Book</a><br> 
          <a class="menuitem" id="daybookview">View by Date</a><br>
          <a class="menuitem" id="daybookviewbillno">View by Bill Number</a><br>
        <a class="menuhead" id="recordhead">Beat Record </a><br> 
          <a class="menuitem" id="recordview">View By Beat</a><br>
          <a class="menuitem" id="recordviewshop">View By Shop</a><br>
        <a class="menuhead" id="beathead">Beat </a><br>
          <a class="menuitem" id="beatview">View</a><br>
        <a class="menuhead" id="shophead">Shop </a><br>
          <a class="menuitem" id="shopview">View</a><br>
        <a class="menuhead" id="collectionhead">Employee</a><br>
          <a class="menuitem" id="collectionview">View</a><br>
    </div>

    <div id="innercontainer">
    	<div id="content">
        	<h1>Content</h1>
    	</div>

    	<div id="sidebar">
    	</div>
    	<div id="result">
    	</div>
    	<div id="result2">
    	</div>
    </div>

    <div id="footer"  style="clear:both;text-align:center;">
        Swasthik Software Solutions, Thanjavur <br>
                   Copyrights 2016
    </div>
</div>

<script src="beat.js"></script>
<script src="collection.js"></script>
<script src="shop.js"></script>
<script src="record.js"></script>
<script src="daybook.js"></script>
<script src="analysis.js"></script>
<script src="jquery.min.js"></script>
<script src="jquery-ui.min.js"></script>
<script>
  $(document).ready(function() {
    $("#datepicker").datepicker();
  });
  $(document).ready(function() {
    $("#datepicker1").datepicker();
  });
  $(document).ready(function() {
    $("#datepicker2").datepicker();
  });
  $(document).ready(function() {
    $("#datepicker3").datepicker();
  });
  $(document).ready(function() {
    $("#datepicker4").datepicker();
  });
  $(document).ready(function() {
    $("#datepicker5").datepicker();
  });
  $(document).ready(function() {
    $("#datepicker6").datepicker();
  });
  $(document).ready(function() {
    $("#datepicker7").datepicker();
  });
  $(document).ready(function() {
    $("#datepicker8").datepicker();
  });
  $(document).ready(function() {
    $("#datepicker9").datepicker();
  });
  $(document).ready(function() {
    $("#datepicker10").datepicker();
  });
</script>
</body>
</html> 
