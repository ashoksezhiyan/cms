document.getElementById("recordview").onclick=function(){recordviewFunc(this)};

function recordviewFunc(id) {
    var xmlhttp;

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("GET","record/recordview.php",true);
    xmlhttp.send();
}

/* function to handle Beat Addition Form
   we have to return false in this function
   to avoid form submit action not to happen
   Required action is already taken in this
   onsubmit event
*/
function viewrecord(id) {
    var xmlhttp;
    var text = "";

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("result").innerHTML=xmlhttp.responseText;
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("POST","record/recordviewfromDB.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    text += ("beatname=" + id.options[id.selectedIndex].value);
    xmlhttp.send(text);
    return false;
} 

document.getElementById("recordviewshop").onclick=function(){recordviewshopFunc(this)};

function recordviewshopFunc(id) {
    var xmlhttp;

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("GET","record/recordviewshop.php",true);
    xmlhttp.send();
}

/* function to handle Beat Addition Form
   we have to return false in this function
   to avoid form submit action not to happen
   Required action is already taken in this
   onsubmit event
*/
function viewshoprecord(id) {
    var xmlhttp;
    var text = "";

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("result").innerHTML=xmlhttp.responseText;
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("POST","record/recordviewshop2.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    text += ("beatname=" + id.options[id.selectedIndex].value);
    xmlhttp.send(text);
    return false;
} 

function viewshoprecord2(id, var1) {
    var xmlhttp;
    var text = "";

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("result2").innerHTML=xmlhttp.responseText;
        }
    }
    xmlhttp.open("POST","record/recordviewshop3.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    text += ("shopname=" + id.options[id.selectedIndex].value + "&" );
    text += ("beatname=" + var1);
    xmlhttp.send(text);
    return false;
} 

document.getElementById("recordadd").onclick=function(){recordaddFunc(this)};

function recordaddFunc(id) {
    var xmlhttp;

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
            document.getElementById("result2").innerHTML="";
            $("#datepicker").datepicker();
        }
    }
    xmlhttp.open("GET","record/recordadd.php",true);
    xmlhttp.send();
}

/* function to handle Beat Addition Form
   we have to return false in this function
   to avoid form submit action not to happen
   Required action is already taken in this
   onsubmit event
*/
function addnewrecord(id) {
    var xmlhttp;
    var text = "";

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("POST","record/recordaddtoDB.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    for (i = 0; i < id.length; i++) {
        text += id.elements[i].name + "=";
        text += id.elements[i].value + "&";
    }
    xmlhttp.send(text);
    return false;
} 


document.getElementById("recordcashadd").onclick=function(){recordcashaddFunc(this)};

function recordcashaddFunc(id) {
    var xmlhttp;

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
            $("#datepicker").datepicker();
        }
    }
    xmlhttp.open("GET","record/recordcashadd.php",true);
    xmlhttp.send();
}

/* function to handle Beat Addition Form
   we have to return false in this function
   to avoid form submit action not to happen
   Required action is already taken in this
   onsubmit event
*/
function addnewcashrecord(id) {
    var xmlhttp;
    var text = "";

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("POST","record/recordcashaddtoDB.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    for (i = 0; i < id.length; i++) {
        text += id.elements[i].name + "=";
        text += id.elements[i].value + "&";
    }
    xmlhttp.send(text);
    return false;
} 

document.getElementById("recordupdate").onclick=function(){recordupdateFunc(this)};

function recordupdateFunc(id) {
    var xmlhttp;

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("GET","record/recordupdate.php",true);
    xmlhttp.send();
}

/* function to handle Beat Addition Form
   we have to return false in this function
   to avoid form submit action not to happen
   Required action is already taken in this
   onsubmit event
*/
function updaterecord(id) {
    var xmlhttp;
    var text = "";

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("result").innerHTML=xmlhttp.responseText;
            document.getElementById("result2").innerHTML="";
            $("#datepicker").datepicker();
        }
    }
    xmlhttp.open("POST","record/recordupdatetoDB.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    //text += ("Billnumber=" + id[0].value);
    for (i = 0; i < id.length; i++) {
        text += id.elements[i].name + "=";
        text += id.elements[i].value + "&";
    }
    xmlhttp.send(text);
    return false;
} 

function updaterecordtodb(id) {
    var xmlhttp;
    var text = "";

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("result").innerHTML=xmlhttp.responseText;
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("POST","record/recordupdatetoDB2.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    //text += ("Billnumber=" + id[0].value);
    for (i = 0; i < id.length; i++) {
        text += id.elements[i].name + "=";
        text += id.elements[i].value + "&";
    }
    xmlhttp.send(text);
    return false;
} 
document.getElementById("recordeditbillno").onclick=function(){recordeditbillnoFunc(this)};

function recordeditbillnoFunc(id) {
    var xmlhttp;

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("GET","record/recordeditbillno.php",true);
    xmlhttp.send();
}
/* function to handle Beat Addition Form
   we have to return false in this function
   to avoid form submit action not to happen
   Required action is already taken in this
   onsubmit event
*/ 
function editrecordbybillno(id) {
    var xmlhttp;
    var text = "";

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("result").innerHTML=xmlhttp.responseText;
            $("#datepicker").datepicker();
            $("#datepicker1").datepicker();
            $("#datepicker2").datepicker();
            $("#datepicker3").datepicker();
            $("#datepicker4").datepicker();
            $("#datepicker5").datepicker();
            $("#datepicker6").datepicker();
            $("#datepicker7").datepicker();
            $("#datepicker8").datepicker();
            $("#datepicker9").datepicker();
            $("#datepicker10").datepicker();
        }
    }
    xmlhttp.open("POST","record/recordeditbillnofromDB.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    for (i = 0; i < id.length; i++) {
        text += id.elements[i].name + "=";
        text += id.elements[i].value + "&";
    }
    xmlhttp.send(text);
    return false;
} 
/* function to handle Beat Addition Form
   we have to return false in this function
   to avoid form submit action not to happen
   Required action is already taken in this
   onsubmit event
*/ 
function editrecordbybillno2(id) {
    var xmlhttp;
    var text = "";

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("POST","record/recordeditbillnofromDB2.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    for (i = 0; i < id.length; i++) {
        text += id.elements[i].name + "=";
        text += id.elements[i].value + "&";
    }
    xmlhttp.send(text);
    return false;
} 
document.getElementById("recorddeletebillno").onclick=function(){recorddeletebillnoFunc(this)};

function recorddeletebillnoFunc(id) {
    var xmlhttp;

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("GET","record/recorddeletebillno.php",true);
    xmlhttp.send();
}
/* function to handle Beat Addition Form
   we have to return false in this function
   to avoid form submit action not to happen
   Required action is already taken in this
   onsubmit event
*/ 
function deleterecordbybillno(id) {
    var xmlhttp;
    var text = "";

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("result").innerHTML=xmlhttp.responseText;
        }
    }
    xmlhttp.open("POST","record/recorddeletebillnofromDB.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    for (i = 0; i < id.length; i++) {
        text += id.elements[i].name + "=";
        text += id.elements[i].value + "&";
    }
    xmlhttp.send(text);
    return false;
} 

/*document.getElementById("recorddel").onclick=function(){recorddelFunc(this)};

function recorddelFunc(id) {
    var xmlhttp;

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
        }
    }
    xmlhttp.open("GET","record/recorddel.php",true);
    xmlhttp.send();
}

/ * function to handle Beat Addition Form
   we have to return false in this function
   to avoid form submit action not to happen
   Required action is already taken in this
   onsubmit event
* / 
function delrecord(id) {
    var xmlhttp;
    var text = "";

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
        }
    }
    xmlhttp.open("POST","record/recorddelfromDB.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    for (i = 0; i < id.length; i++) {
        text += id.elements[i].name + "=";
        text += id.elements[i].value + "&";
    }
    xmlhttp.send(text);
    return false;
} 

*/
