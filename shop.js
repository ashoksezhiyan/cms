document.getElementById("shopview").onclick=function(){shopviewFunc(this)};

function shopviewFunc(id) {
    var xmlhttp;

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("GET","shop/shopview.php",true);
    xmlhttp.send();
}

/* function to handle Beat Addition Form
   we have to return false in this function
   to avoid form submit action not to happen
   Required action is already taken in this
   onsubmit event
*/
function viewshop(id) {
    var xmlhttp;
    var text = "";

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("result").innerHTML=xmlhttp.responseText;
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("POST","shop/shopviewfromDB.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    text += ("beatname=" + id.options[id.selectedIndex].value);
    xmlhttp.send(text);
    return false;
} 

document.getElementById("shopadd").onclick=function(){shopaddFunc(this)};

function shopaddFunc(id) {
    var xmlhttp;

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("GET","shop/shopadd.php",true);
    xmlhttp.send();
}

/* function to handle Beat Addition Form
   we have to return false in this function
   to avoid form submit action not to happen
   Required action is already taken in this
   onsubmit event
*/
function addnewshop(id) {
    var xmlhttp;
    var text = "";

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("POST","shop/shopaddtoDB.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    for (i = 0; i < id.length; i++) {
        text += id.elements[i].name + "=";
        text += id.elements[i].value + "&";
    }
    xmlhttp.send(text);
    return false;
} 

document.getElementById("shopdel").onclick=function(){shopdelFunc(this)};

function shopdelFunc(id) {
    var xmlhttp;

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("GET","shop/shopdel.php",true);
    xmlhttp.send();
}

/* function to handle Beat Addition Form
   we have to return false in this function
   to avoid form submit action not to happen
   Required action is already taken in this
   onsubmit event
*/
function delshop(id) {
    var xmlhttp;
    var text = "";

    xmlhttp=new XMLHttpRequest();

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("content").innerHTML=xmlhttp.responseText;
            document.getElementById("result").innerHTML="";
            document.getElementById("result2").innerHTML="";
        }
    }
    xmlhttp.open("POST","shop/shopdelfromDB.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    for (i = 0; i < id.length; i++) {
        text += id.elements[i].name + "=";
        text += id.elements[i].value + "&";
    }
    xmlhttp.send(text);
    return false;
} 
